<?php
/* developer : Aiyash Ahmed (full-stack developer),
 * email : aiyashahmed96@gmail.com,
 * data : 2020/10/17,
 * country : Sri lanka
 * */

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// landing page
Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@aboutus')->name('about');
Route::get('/contact', 'HomeController@contactus')->name('contact');
Route::get('/verification', 'HomeController@verification')->name('verification');

Route::get('/faculties', 'HomeController@faculties')->name('faculties');
Route::get('/faculty/{faculty}', 'HomeController@faculty')->name('faculty');

Route::get('/recognition', 'HomeController@recognition')->name('recognition');
Route::get('/admission', 'HomeController@admission')->name('admission');
Route::get('/application', 'HomeController@onlineapplication')->name('application');
Route::get('/entry-requirements', 'HomeController@entryRequirements')->name('entry-requirements');
Route::get('/download', 'HomeController@downloadView')->name('download');
Route::get('/refund-policy', 'HomeController@refundPolicy')->name('refund-policy');

//make application
Route::post('/application', 'ApplicationController@add')->name('store.application');

//send message
Route::post('/contact/message', 'HomeController@sendMessage')->name('store.message');

// admin page
Route::group(["prefix" => "admin"], function () {
    Route::get('/', 'Auth\AuthController@index')->name('admin');
    Route::post('/login', 'Auth\AuthController@login');
    Route::get('/logout', 'Auth\AuthController@logout')->name('logout');

    Route::group(['middleware' => ['admin']], function () {
        Route::get('/dashboard', 'AdminController@dashboard');

        Route::get('/verification','AdminController@verification')->name('admin.verification');
        Route::post('/verification', 'AdminController@addVerification');
        Route::get('/verification/datatable', 'AdminController@verificationTable');
        Route::post('verification/delete', 'AdminController@deleteVerification');
        Route::get('verification/edit/{id}', 'AdminController@editVerification');
        Route::post('verification/update/{id}', 'AdminController@updateVerification')->name('verification.update');

//        pdf forms
        Route::get('/files', 'AdminController@form');
        Route::post('/files/upload-file', 'AdminController@uploadFile');
        Route::get('/files/datatable', 'AdminController@filesTable');
        Route::post('/files/delete', 'AdminController@deleteFiles');

//        application
        Route::get('/application', 'ApplicationController@table')->name('admin.application');
        Route::get('/application/datatable', 'ApplicationController@dataTable');
        Route::post('/application/delete', 'ApplicationController@deleteRow');


        //        messages
        Route::get('/messages', 'MessageController@table')->name('admin.messages');
        Route::get('/messages/datatable', 'MessageController@dataTable');
        Route::post('/messages/delete', 'MessageController@deleteRow');

//        user setting
        Route::post('/dashboard/setting', "AdminController@changeSetting");
        Route::post('/verification/setting', "AdminController@changeSetting");
        Route::post('/files/setting', "AdminController@changeSetting");
        Route::post('/application/setting', "AdminController@changeSetting");
        Route::post('/messages/setting', "AdminController@changeSetting");
    });
});

// verify no
Route::post('/verification/verify', 'AdminController@verify')->name('verify');
Route::get('/download/{id}', 'AdminController@getDownload')->name('download.lettter');
