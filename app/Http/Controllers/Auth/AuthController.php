<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Login;
use Auth;

class AuthController extends Controller
{
    public function index(){

        if(Auth::user()) return view("backend.dashboard");
        return view('backend.login');
    }

    public function login(Request $req){
        /**
         * send credentials and remember token
         * returns the response in an array
         */
        $remember = $req->rememberCheck == 'on' ? true : false;
        $login = new Login;
        $response = $login->credentials($req->all(),$remember);
        return $response;
    }

    public function logout() {
        Auth::logout();
        return redirect('/admin');
    }
}
