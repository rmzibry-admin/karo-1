<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use App\Application;

class ApplicationController extends Controller
{
    public function add(Request $req){

        $req->validate([
            'programme' => 'required',
            'fname' => 'required',
            'nic_passport'=> 'required',
            'gender'=> 'required',
            'address'=> 'required',
            'postal_code'=> 'required',
            'country'=> 'required',
            'contact'=> 'required',
            'email'=> 'required',
            'dob'=> 'required'
        ]);

        $error = 1;
        $add = DB::table('application')->insert([
            'programme' => "$req->programme",
            'fname' => "$req->fname",
            'nic_passport' => "$req->nic_passport",
            'gender' => "$req->gender",
            'address' => "$req->address",
            'city' => "$req->city",
            'postal_code' => "$req->postal_code",
            'country' => "$req->country",
            'contact' => "$req->contact",
            'email' => "$req->email",
            'dob' => "$req->dob"
        ]);

        if($add){
            $error = 0;
        }

        return response([
            'status' => 1,
            'message' => 'Application Applied Succesfully..!'
        ]);
    }

    public function table()
    {
        Application::query()->update(['flag' => 1]);

        return view('backend.application');
    }

    public function dataTable(){

        $data = DB::table('application')->get();
        return Datatables::of($data)->make(true);
    }

    public function deleteRow(Request $req){
        $id = $req->id;

        $delete = DB::table("application")->where('id',$id)->delete();
        if ($delete) {
            return response([
                'delete' => 1
            ]);
        }
    }
}
