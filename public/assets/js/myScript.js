$(function () {
    //Date picker
    $('#datepicker').datepicker({
        autoclose: true
    })

    //Date picker
    $('#application-dob').datepicker({
        autoclose: true
    })

//    verification datatable
    $verificationTable = $("#verification-table").DataTable({
        processing: true,
        serverSide: true,
        ajax: $base_url + "/datatable",
        columns: [
            {data: "f_name"},
            {data: "l_name"},
            {data: "learner_no"},
            {data: "certificate_no"},
            {data: "course"},
            {data: "gpa"},
            {data: "country"},
            {
                render: function (data, type, full, meta) {

                    var url = $base_url+'/../../pdfs/' + full.pdf;
                    if(full.pdf)
                    return (
                        '<a class="btn btn-primary" href="' +
                        url +
                        '" class="mt-5" download> <i class="fa fa-download" aria-hidden="true"></i></a>'
                    );

                    return '';
                }
            },
            {
                render: function (data, type, full, meta) {

                    var url = $base_url+'/../../images/verifications/' + full.image;
                    if(full.image)
                    return (
                        '<img class="img-thumbnail w-75" src="' +
                        url +
                        '">'
                    );

                    return '';
                }
            },
            {
                render: function (data, type, full, meta) {

                    var urlEdit = $base_url + "/edit/";
                    return (
                        '<a class="btn btn-xs btn-outline-primary mb-2" id="' +
                        full.id +
                        '" href="'+ urlEdit + full.id +'"><i class="fas fa-edit text-red"></i></a>'+
                        '<button class="btn btn-xs btn-outline-danger verification-delete-btn" id="' +
                        full.id +
                        '"><i class="fas fa-trash text-red"></i></button>'

                    );
                },
            },
        ],
    });

//    verification form search
    $("#verification-form").submit(function (e) {
        e.preventDefault();
        const form = "#verification-form";
        const btn = $("#search-no-btn");
        btn.text("Loading..");
        const number = $(form + " input[name='certificate_no']").val();
        $("#certificate_info_form input").val("");
        if (number == "") {
            $("#check-no-error").fadeIn().text("Verification number is empty");
            btn.text("Search");
        } else {
            var url = $base_url + "/verify";
            var data = {"no": number};
            var type = "post";

            ajPost(url, data, type, function (r) {
                if (r.available == 0) {
                    $("#check-no-error").fadeIn().text("Invalid number");
                } else {
                    $("#check-no-error").fadeOut();
                    $.each(r.data, function (k, v) {
                        $("#" + k).val(v);
                    })
                }

                btn.text("Search");
            })
        }

    })

//    login
    $('#login-form').submit(function (e) {
        e.preventDefault();
        var btn = $("#login-form button");
        var url = $base_url + "/login";
        var data = $(this).serialize();
        var type = "post";

        btn.text("loading..");

        ajPost(url, data, type, function (r) {
            if (r.error == 0) {
                location.href = $base_url + '/dashboard';
            } else {
                $("#login-error").fadeIn().text(r.msg + "!");

                setTimeout(function () {
                    $("#login-error").fadeOut();
                }, 4000)
            }

            btn.text('Login');
        })
    })

//    add new verification
    $("#add-verification-form").submit(function (e) {
        e.preventDefault();

        var formData = new FormData($(this)[0]);

        var btn = $("#submit-btn");
        btn.text("Loading..");
        var url = $base_url;
        var type = "post";
        var data = formData;
        ajPost2(url, data, type, function (r) {
            if (r.error == 1) {
                $.each(r.msg, function (k, v) {
                    $("#" + k).fadeIn().text(v);
                })

                setTimeout(function () {
                    $(".error-badge").fadeOut();
                }, 3000)
            } else {
                $("#add-new-verification-modal #success-msg").show();
                $("#add-new-verification-modal #success-msg").fadeIn().text("Successfully Added !");
                $verificationTable.ajax.reload();
                $("#add-verification-form input").val("");
                setTimeout(function () {
                    $("#add-new-verification-modal #success-msg").fadeOut();
                }, 1000)
            }
            btn.text("Submit");
        })
    })

    $("#edit-verification-form").submit(function (e) {
        e.preventDefault();

        var formData = new FormData($(this)[0]);

        var btn = $("#submit-btn");
        btn.text("Loading..");
        var url = $(this).attr("action");
        var type = "post";
        var data = formData;
        ajPost(url, data, type, function (r) {
            if (r.error == 1) {
                $.each(r.msg, function (k, v) {
                    $("#" + k).fadeIn().text(v);
                })

                setTimeout(function () {
                    $(".error-badge").fadeOut();
                }, 3000)
            } else {
                $("#add-new-verification-modal #success-msg").fadeIn().text("Successfully Added !");
                $verificationTable.ajax.reload();
                $("#add-verification-form input").val("");
                setTimeout(function () {
                    $("#add-new-verification-modal #success-msg").fadeOut();
                }, 1000)
            }
            btn.text("Submit");
        })
    })

//    delete verification
    $("#verification-table").on("click", ".verification-delete-btn", function () {
        var url = $base_url + "/delete";
        var data = {"id": $(this).attr("id")};
        var type = "post";
        var btn = $(this);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.value) {
                btn.text("Deleteing..");
                ajPost(url, data, type, function (r) {
                    if (r.error == 0) {
                        Swal.fire(
                            "Deleted!",
                            "Your file has been deleted.",
                            "success"
                        );
                    } else {
                        Swal.fire(
                            "Error!",
                            "This file can not delete at the moment.",
                            "error"
                        );
                    }

                    $verificationTable.ajax.reload();
                    btn.html("<i class='fa fa-trash'></i>");
                })
            }
        });
    })

//    change user setting
    $("#setting-form").submit(function (r) {
        r.preventDefault();
        var btn = $("#setting-btn");
        var data = $(this).serialize();
        var url = $base_url + "/setting";
        var type = "post";
        var error = $("#error-msg");
        var success = $("#setting-success-msg");
        error.text("");
        success.text("");

        btn.text("Loading..");
        ajPost(url, data, type, function (r) {
            if (r.error == 1) {
                error.text(r.msg);
            } else {
                success.text("Successfully changed");
                $('#setting-form .form-control').val("");
                setTimeout(function () {
                    $("#setting-modal").modal("hide");
                }, 1500);
            }

            btn.text("Change");
        })
    })

    //    files table
    $filesTable = $("#files-table").DataTable({
        processing: true,
        serverSide: true,
        ajax: $base_url + "/datatable",
        columns: [
            {data: "name"},
            {
                render: function (data, type, full, meta) {
                    return (
                        '</button> <button class="btn btn-xs btn-outline-danger file-delete-btn" id="' +
                        full.id +
                        '"><i class="fas fa-trash text-red"></i></button>'
                    );
                },
            },
        ],
    });

//    pdf file upload
    $("#add-pdf-file").submit(function (r) {
        r.preventDefault();
        var btn = $("#upload-btn");
        var url = $base_url + "/upload-file";
        var type = "post";

        btn.text("Uploading..");
        $(".error-badge").text("");

        $.ajax({
            url,
            type,
            data: new FormData(this),
            contentType: false,
            cach: false,
            processData: false,
            success: function (r) {
                if (r.error == 1) {
                    $.each(r.msg,function(k,v){
                        $("#"+k).text(v);
                    })
                }else {
                    $("#success-msg").text("Successfully uploaded");
                    $filesTable.ajax.reload();
                    setTimeout(function () {
                        $("#success-msg").text("");
                        $("#add-pdf-file input").val("");
                        $("#add-new-file-modal").modal("hide");
                    },1500);
                }

                btn.html("<i class='fa fa-file-upload'></i>");
            }
        })
    })

    //    delete files
    $("#files-table").on("click", ".file-delete-btn", function () {
        var url = $base_url + "/delete";
        var data = {"id": $(this).attr("id")};
        var type = "post";
        var btn = $(this);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.value) {
                btn.text("Deleteing..");
                ajPost(url, data, type, function (r) {
                    if (r.delete == 1) {
                        Swal.fire(
                            "Deleted!",
                            "Your file has been deleted.",
                            "success"
                        );
                    } else {
                        Swal.fire(
                            "Error!",
                            "This file can not delete at the moment.",
                            "error"
                        );
                    }
                    btn.html("<i class='fa fa-trash'></i>");
                    $filesTable.ajax.reload();
                })
            }
        });
    })

//    apply form
    $("#application-form").submit(function(r){
        r.preventDefault();
        var error = false;
        var btn = $("#apply-btn");
        $("#application-form input").each(function(i){
            var id = $(this).attr('name');
            if($(this).val() == ""){
                error = true;

                $("#"+id).fadeIn().text("This field can not be empty");

                setTimeout(function(){
                    $("#"+id).fadeOut();
                },3000);
            }
        })

        if(!error){
            btn.text("Loading..");
            var url = $base_url+'/apply';
            var data = $("#application-form").serialize();
            var type = "post";

            $("#success-application label").fadeOut();

            ajPost(url,data,type,function(r){
                if(r.error == 0){
                    //    no error
                    console.log('coming');
                    $("#application-form input").val("");
                    $("#success-application label").fadeIn().text("Thank you for applying. We'll get back to you soon.");
                    setTimeout(function(){
                        $("#success-application label").fadeOut();
                    },5000);

                }else{
                    console.log("Something went wrong");
                }

                btn.text("Apply");
            })
        }
    })

    $applicationTable = $("#application-table").DataTable({
        processing: true,
        serverSide: true,
        ajax: $base_url + "/datatable",
        columns: [
            {data: "programme"},
            {data: "fname"},
            {data: "nic_passport"},
            {data: "dob"},
            {data: "gender"},
            {data: "address"},
            {data: "postal_code"},
            {data: "country"},
            {data: "contact"},
            {data: "email"},
            {
                render: function (data, type, full, meta) {
                    return (
                        '</button> <button class="btn btn-xs btn-outline-danger application-delete-btn" id="' +
                        full.id +
                        '"><i class="fas fa-trash text-red"></i></button>'
                    );
                },
            },
        ],
    });

    //    delete application
    $("#application-table").on("click", ".application-delete-btn", function () {
        var url = $base_url + "/delete";
        var data = {"id": $(this).attr("id")};
        var type = "post";
        var btn = $(this);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.value) {
                btn.text("Deleteing..");
                ajPost(url, data, type, function (r) {
                    if (r.delete == 1) {
                        Swal.fire(
                            "Deleted!",
                            "Your file has been deleted.",
                            "success"
                        );
                    } else {
                        Swal.fire(
                            "Error!",
                            "This file can not delete at the moment.",
                            "error"
                        );
                    }
                    btn.html("<i class='fa fa-trash'></i>");
                    $applicationTable.ajax.reload();
                })
            }
        });
    })

    //    message form
    $("#contact-form").submit(function(r){
        r.preventDefault();
        var error = false;
        var btn = $("#contact-btn");
        $("#contact-form input").each(function(i){
            var id = $(this).attr('name');
            if($(this).val() == ""){
                error = true;

                $("#"+id).fadeIn().text("This field can not be empty");

                setTimeout(function(){
                    $("#"+id).fadeOut();
                },3000);
            }
        })

        if($("#contact-form textarea").val() == ""){
            var id = $(this).attr('name');
            $("#"+id).fadeIn().text("This field can not be empty");

            setTimeout(function(){
                $("#"+id).fadeOut();
            },3000);
        }

        if(!error){
            btn.text("Loading..");
            var url = $base_url+'/message';
            var data = $("#contact-form").serialize();
            var type = "post";

            $("#success-application label").fadeOut();

            ajPost(url,data,type,function(r){
                if(r.error == 0){
                    //    no error
                    console.log('coming');
                    $("#contact-form input").val("");
                    $("#contact-form textarea").val("");
                    $("#success-contact label").fadeIn().text("Thank you. We'll get back to you soon.");
                    setTimeout(function(){
                        $("#success-contact label").fadeOut();
                    },5000);

                }else{
                    console.log("Something went wrong");
                }

                btn.text("Send Message");
            })
        }
    })

    // message datatable
    $messageTable = $("#message-table").DataTable({
        processing: true,
        serverSide: true,
        ajax: $base_url + "/datatable",
        columns: [
            {data: "date"},
            {data: "fname"},
            // {data: "lname"},
            {data: "email"},
            {data: "subject"},
            {data: "message"},
            {data: "phone"},
            {
                render: function (data, type, full, meta) {
                    return (
                        '</button> <button class="btn btn-xs btn-outline-danger message-delete-btn" id="' +
                        full.id +
                        '"><i class="fas fa-trash text-red"></i></button>'
                    );
                },
            },
        ],
    });

    //    delete messages
    $("#message-table").on("click", ".message-delete-btn", function () {
        var url = $base_url + "/delete";
        var data = {"id": $(this).attr("id")};
        var type = "post";
        var btn = $(this);

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.value) {
                btn.text("Deleteing..");
                ajPost(url, data, type, function (r) {
                    if (r.delete == 1) {
                        Swal.fire(
                            "Deleted!",
                            "Your file has been deleted.",
                            "success"
                        );
                    } else {
                        Swal.fire(
                            "Error!",
                            "This file can not delete at the moment.",
                            "error"
                        );
                    }
                    btn.html("<i class='fa fa-trash'></i>");
                    $messageTable.ajax.reload();
                })
            }
        });
    })
})

function ajPost(url, data, type, callback) {
    $.ajax({
        url,
        type,
        data,
        dataType: "json",
        success: function (r) {
            callback(r);
        }
    })
}

function ajPost2(url, data, type, callback) {
    $.ajax({
        url,
        type,
        data,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (r) {
            callback(r);
        }
    })
}

