@extends('backend.layouts.master')

@section('heading')
@endsection

@section('add-new-btn')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Messages
                </div>
                <div class="card-body table-responsive">
                    <table id="message-table" class="table table-borderless">
                        <thead>
                        <th> Date</th>
                        <th> First Name</th>
                        {{-- <th> Last Name</th> --}}
                        <th> Email</th>
                        <th> Subject</th>
                        <th> Message</th>
                   
                        <th> Phone</th>
                        <th> Action</th>
                        </thead>
                        <tbody class="text-capitalize">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--    add new model --}}

@endsection
