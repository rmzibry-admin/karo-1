@extends('backend.layouts.master')

@section('heading')

@endsection

@section('content')
    <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
            <a href="{{ route('admin.verification') }}">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Verifications </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $verifications ?? 0 }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-graduation-cap fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        {{-- @if (isset($new_verifications))
                            <span class="badge  badge-pill badge-success float-right mt-3">{{ $new_verifications ?? '' }}
                                New</span>
                        @endif --}}
                    </div>
                </div>
            </a>
        </div>

        <div class="col-xl-4 col-md-6 mb-4">
            <a href="{{ route('admin.messages') }}">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Messages </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $messages ?? 0 }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comment fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        @if (!empty($new_messages))
                            <span class="badge  badge-pill badge-success float-right mt-3">{{ $new_messages ?? '' }} New</span>
                        @endif
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <a href="{{ route('admin.application') }}">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Applications </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $applications ?? 0 }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-file fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        @if (!empty($new_applications))
                            <span class="badge  badge-pill badge-success float-right mt-3">{{ $new_applications ?? '' }}
                                New</span>
                        @endif
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
