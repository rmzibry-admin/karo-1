@extends('front.layouts.app')

@section('content')
    <!--====== PAGE BANNER PART START ======-->

    <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8"
        style="background-image: url({{ url('front_assets/images/page-banner-5.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-cont">
                        <h2>Admissions</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Admissions</li>
                            </ol>
                        </nav>
                    </div> <!-- page banner cont -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== PAGE BANNER PART ENDS ======-->

    <!--====== ENTRY REQ START ======-->

    <section id="event-singel" class="pt-30 pb-0 gray-bg">
        <div class="container">
            <div class="events-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="events-left"></div>


                        <h3>01. Entry Requirements</h3> <br>

                        <p class="font-weight-bold">Candidates for Admission to all graduate programs must:</p>
                        <br>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">Submit a 1-2 page typed narrative outlining
                                one’s intention <br> (e.g., reasons for undertaking a college degree, professional
                                aspirations, etc.).</li>
                            <li class="list-group-item list-group-item-action">Duly signed and fully completed the
                                Application Form for Admissions</li>
                            <li class="list-group-item list-group-item-action">Copies of all educational Certificates</li>
                            <li class="list-group-item list-group-item-action">A Details CV (Bio Data)</li>
                            <li class="list-group-item list-group-item-action">A Passport Size Color Photos</li>
                            <li class="list-group-item list-group-item-action">Copies of Working Experience Letters</li>
                            <li class="list-group-item list-group-item-action">Passport Detail Page Copy</li>
                            <li class="list-group-item list-group-item-action">A Confirmation Letter from the Approved
                                Center</li>
                            <li class="list-group-item list-group-item-action">Two Academic Reference Letters</li>
                        </ul> <br><br>

                        <h3>02. Refund Policy</h3> <br>
                        <p class="font-weight-bold">Students who find it necessary to withdraw from the program must submit
                            a written notice to the University. </p>
                        <br>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">After completing more than 10% of the total
                                module assignments and up to and including completion of 25% of the assignments, 25% of the
                                refundable tuition.</li>
                            <li class="list-group-item list-group-item-action">After completing more than 25% of the total
                                module assignments and up to and including completion of 50% of the assignments, 10% of the
                                refundable tuition.</li>
                            <li class="list-group-item list-group-item-action">If the student completes more than half of
                                the total module assignments or the enrollment period has expired, the school will be
                                entitled to the full tuition and no refund will be able. The amount of the module completed
                                will be the ratio of completed assignments received by the University to the total
                                assignments required to complete the module.</li>
                        </ul> <br><br>

                        <h3>03. Non-Discrimination Policy</h3> <br>

                        <p class="font-weight-bold">Equal enrollment and attendance standards.</p>
                        <p class="text-justify">
                            Universidad of Unikaro pledges to equal opportunity and non-discrimination policies with absence
                            of racial, gender, creed or social preference. However, students must agree to comply with the
                            admission requirements and institutional principles, norms and policies in effect. Continuous
                            attendance is subject to the fulfillment of the same conditions.<br><br>
                            Universidad of Unikaro seeks to foster social mobility through its financial aid programs and
                            scholarship funds, made available to high-achieving students with proven economic need. Social
                            mobility is also promoted through the creation of educational models aimed at reducing the
                            education gap in target regions or countries. <br><br>
                        </p>

                        <p class="font-weight-bold">Equal employment opportunity and continuous employment standards.</p>
                        <p class="text-justify">
                            Universidad of Unikaro is an equal opportunity employer with non-discrimination policies and
                            absence of racial, gender, creed or social preference. However, applicants must demonstrate the
                            professional and moral standards required for the position, as well as comply with the
                            institutional principles, norms and policies in effect. Continuous employment is subject to the
                            fulfillment of the same conditions. <br><br>
                        </p>

                        <p class="font-weight-bold">Academic Freedom.</p>
                        <p class="text-justify">
                            Universidad of Unikaro fosters plurality of thought and cultural diversity and, thus, its
                            faculty’s academic freedom. This right implies fulfilling the academic programs in effect and
                            observing the established research norms. <br><br>
                        </p>

                        <p class="font-weight-bold">Academic freedom is understood as:</p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">The freedom to teach, this is, a professor’s
                                right to present the results of his/her research findings and studies, and the different
                                schools of thought and analysis relevant to the discipline</li>
                            <li class="list-group-item list-group-item-action">The freedom to perform research, in agreement
                                with Universidad of Unikaro mission’s aim, and to publish its findings. </li>
                            <li class="list-group-item list-group-item-action">Academic freedom implies the responsibility
                                to refrain from using research as a proselytizing instrument in favor of any ideological,
                                religious or political group.</li>
                        </ul>

                    </div> <!-- events left -->
                </div>

            </div> <!-- events right -->
        </div>
        </div> <!-- row -->
        </div> <!-- events-area -->
        </div> <!-- container -->
    </section>

    <!--====== ENTRY REQ PART ENDS ======-->
@endsection
