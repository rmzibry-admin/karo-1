@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(f-assets/images/banner/banner1.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Recognitions</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Recognitions</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- contact area -->
        <div class="content-block">
            <!-- Portfolio  -->
            <div class="section-area section-sp1 gallery-bx">
                <div class="container">
                    <div class="clearfix">
                        <h2 class="title-head">Our <span>Recognitions</span></h2>
                        <p>Our professional accreditations and partnerships.</p>
                        <ul id="masonry" class="ttr-gallery-listing magnific-image row">
                            <li class="action-card col-lg-3 col-md-4 col-sm-6 liststyle">
                                <div class="ttr-box portfolio-bx">
                                    <div class="ttr-media media-effect">
                                        <a href="javascript:void(0);">
                                            <img src="{{ asset('f-assets/images/recognitions/Logo_IAU.png') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="action-card col-lg-3 col-md-4 col-sm-6 liststyle">
                                <div class="ttr-box portfolio-bx">
                                    <div class="ttr-media media-effect">
                                        <a href="javascript:void(0);">
                                            <img src="{{ asset('f-assets/images/recognitions/Logo_WHED.png') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="action-card col-lg-3 col-md-4 col-sm-6 liststyle">
                                <div class="ttr-box portfolio-bx">
                                    <div class="ttr-media media-effect">
                                        <a href="javascript:void(0);">
                                            <img src="{{ asset('f-assets/images/recognitions/unesco.png') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="action-card col-lg-3 col-md-4 col-sm-6 liststyle">
                                <div class="ttr-box portfolio-bx">
                                    <div class="ttr-media media-effect">
                                        <a href="javascript:void(0);">
                                            <img src="{{ asset('f-assets/images/recognitions/ISO_9001-2015.png') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->
@endsection
