@extends('front.layouts.app')

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(../f-assets/images/banner/banner2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Faculty of Business & Social Sciences</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('faculties') }}">Faculties</a></li>
                    <li>Faculty of Business & Social Sciences</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
            <div class="section-area section-sp1">
                <div class="container">
                    <div class="row d-flex flex-row-reverse">
                        <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                            <div class="course-detail-bx">
                                <div class="course-buy-now text-center">
                                    <a href="{{ route('application') }}" class="btn radius-xl text-uppercase">Apply Online</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <div class="courses-post">
                                <div class="ttr-post-info">
                                    <div class="ttr-post-title ">
                                        <h2 class="post-title">Faculty of Business & Social Sciences</h2>
                                    </div>
                                    <div class="ttr-post-text">
                                        <p>Welcome to the Faculty of Business and Social Sciences. There is a high demand
                                            for Business and Social Science graduates in today's dynamic world.
                                            Qualifications in these areas open doors due to the broad range of valuable
                                            transferable skills. Graduates of our Faculty are highly sought after, working
                                            at the highest levels across a wide range of industries and sectors both
                                            nationally and internationally.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="m-b30" id="curriculum">
                                <h4>Courses</h4>
                                <ul class="curriculum-list">
                                    <li>
                                        <ul>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>1.</span> Accounting / Finance
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>2.</span> Anthropology / Archaeology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>3.</span> Business / Commerce / Management
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>4.</span> Communication and Media Studies
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>5.</span> Development Studies
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>6.</span> Economics
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>7.</span> Geography
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>8.</span> Law
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>9.</span> Library and Information Science
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>10.</span> Physical Education / Sport Science
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>11.</span> Political and International Studies
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>12.</span> Social Policy / Public Administration
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>13.</span> Social Work
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>14.</span> Sociology / Psychology
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>15.</span> Tourism / Hospitality
                                                </div>
                                            </li>
                                            <li>
                                                <div class="curriculum-list-box">
                                                    <span>16.</span> Other Business & Social Science Studies
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->

    </div>
    <!-- Content END-->
@endsection
